const doImportFromPubMed = require('./sources/pubmed.js')
const doImportFromEuropePMC = require('./sources/europePMC.js')
const doImportFromScopus = require('./sources/scopus.js')

const setImportersForPubMed = broker => {
  const pubmedImportFromYear = 1962
  const currentYear = new Date().getFullYear()

  // eslint-disable-next-line no-plusplus
  for (let year = pubmedImportFromYear; year <= currentYear; year++) {
    const startDate = new Date(year, 0, 1)
    const endDate = new Date(year, 11, 31)
    const workerArgs = { startDate, endDate }

    broker.addManuscriptImporter(
      'any', // 'manual', 'automatic' or 'any'
      doImportFromPubMed(
        broker.findManuscriptWithUri,
        broker.findManuscriptWithDoi,
        broker.getStubManuscriptObject,
        broker.logger,
        workerArgs,
      ),
    )
  }
}

const setImportersForEuropePMC = broker => {
  const europePMCImportFromYear = 2017
  const monthsToRun = 6
  const currentYear = new Date().getFullYear()
  let startDate = new Date(europePMCImportFromYear, 0, 1)

  while (startDate.getFullYear() <= currentYear) {
    const endDate = new Date(
      startDate.getFullYear(),
      startDate.getMonth() + monthsToRun - 1,
      31,
    )

    const workerArgs = { startDate, endDate }

    broker.addManuscriptImporter(
      'any', // 'manual', 'automatic' or 'any'
      doImportFromEuropePMC(
        broker.findManuscriptWithUri,
        broker.findManuscriptWithDoi,
        broker.getStubManuscriptObject,
        broker.logger,
        workerArgs,
      ),
    )

    startDate = new Date(
      startDate.getFullYear(),
      startDate.getMonth() + monthsToRun,
      1,
    )
  }
}

const setImportersForScopus = broker => {
  const scopusImportFromYear = 1956
  const currentYear = new Date().getFullYear()

  // eslint-disable-next-line no-plusplus
  for (let year = scopusImportFromYear; year <= currentYear; year++) {
    const startYear = year
    const endYear = year + 1
    const workerArgs = { startYear, endYear }

    broker.addManuscriptImporter(
      'any', // 'manual', 'automatic' or 'any'
      doImportFromScopus(
        broker.findManuscriptWithUri,
        broker.findManuscriptWithDoi,
        broker.getStubManuscriptObject,
        broker.logger,
        workerArgs,
      ),
    )
  }
}

const pluginEntry = broker => {
  setImportersForPubMed(broker)
  setImportersForEuropePMC(broker)
  setImportersForScopus(broker)
}

module.exports = pluginEntry
