const axios = require('axios')

const apiUrl = 'https://api.crossref.org/v1/works'
const mailTo = 'example@gmail.com'

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
let requiredDelayMs = 20 // Initial rate limit; we will revise this to whatever CrossRef tells us
let earliestPermittedRequestTime = 0

const getDataByDoi = async doi => {
  // Don't exceed the rate limit imposed by CrossRef
  while (Date.now() < earliestPermittedRequestTime)
    // eslint-disable-next-line no-await-in-loop
    await sleep(earliestPermittedRequestTime - Date.now())

  try {
    const requestTime = Date.now()
    earliestPermittedRequestTime =
      Math.max(earliestPermittedRequestTime, requestTime) + requiredDelayMs

    const response = await axios.get(`${apiUrl}/${doi}`, {
      params: {
        mailto: mailTo,
      },
      headers: {
        'User-Agent': `Kotahi (Axios 0.21; mailto:${mailTo})`,
      },
    })

    const rateLimit = parseInt(response.headers['x-rate-limit-limit'], 10)

    const rateLimitInterval = parseInt(
      response.headers['x-rate-limit-interval'].split('s')[0],
      10,
    )

    // Update the rate limit to whatever CrossRef has told us
    requiredDelayMs = (rateLimitInterval * 1000) / rateLimit

    return response.data.message
  } catch (error) {
    console.error(`Resource not found in crossref for DOI ${doi}`)
    return undefined
  }
}

const getPublishedDate = data => {
  const { assertion, published } = data
  const publishedDateData = data['published-online'] || published
  const [year, month, date] = publishedDateData
    ? publishedDateData['date-parts'][0].map(String)
    : []

  if (![year, month, date].length) {
    const publish = assertion.find(a => a.name === 'published')
    return publish.value
  }

  return `${date ? `${date}-` : ''}${month ? `${month}-` : ''}${
    year ? `${year}` : ''
  }`
}

const getTopics = data => {
  const subjects = data.subject
  const groupTitle = data['group-title']

  return subjects || [groupTitle]
}

const getJournal = data => {
  const { institution, publisher } = data
  return institution ? institution[0].name : publisher
}

const getAuthor = data => {
  const authors = data.author
  return authors ? authors[0].given : ''
}

const getCrossrefDataViaDoi = async doi => {
  const data = await getDataByDoi(doi)

  if (!data) return {}

  const { title, abstract, resource } = data
  const publishedDate = getPublishedDate(data)
  const topics = getTopics(data)
  const journal = getJournal(data)
  const author = getAuthor(data)

  return {
    title: title[0],
    topics,
    publishedDate,
    abstract,
    journal,
    author,
    url: resource?.primary.URL,
  }
}

module.exports = {
  getDataByDoi,
  getCrossrefDataViaDoi,
}
