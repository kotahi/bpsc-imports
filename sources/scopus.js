/* eslint-disable no-console */
const axios = require('axios')
const { getCrossrefDataViaDoi } = require('../crossref')

const apiKey = 'ccd79d751c36b2a7d1a1c90feacdc970' // Replace with your API key

const query = `(( TITLE-ABS-KEY ( "zoonoses" OR "zoonotic" OR "zoonosis" OR "spillover*" OR "outbreak*" OR "cross-species" OR "cross species" OR "inter-species" OR "interspecies" ) 
AND 
TITLE-ABS-KEY ( "chiroptera" OR "rhinolophus" OR "rousettus" OR "pteropodidae" OR "pteropus" OR "bat" OR "bats" OR "flying fox*" OR "paramyxoviridae" OR "paramyxovirus*" OR "henipavirus*" OR "morbillivirus*" OR "respirovirus*" OR "rubulavirus*" OR "nipah*" OR "hendra*" OR "kumasi*" OR "sosuga*" OR "orthorubulavirus*" OR "pararubulavirus*" OR "mumps*" OR "menangle*" OR "filoviridae" OR "filovirus*" OR "ebola*" OR "marburg*" OR "reoviridae" OR "reovirus*" OR "orbivirus*" OR "orthoreovirus*" OR "rotavirus*" OR "melaka*" OR "pulau*" OR "rhadboviridae" OR "rhabdovirus*" OR "lyssavirus*" OR "rabies*" OR "duvenhage*" OR "coronaviridae" OR "coronavirus*" OR "alphacoronavirus*" OR "betacoronavirus*" AND NOT ("covid*" OR "sars-cov-2" OR "coronavirus disease 2019*" OR "measles*" OR "peste-des-petits-ruminants*" OR "peste des petits ruminants*" OR "newcastle disease*" OR "rinderpest*" OR "distemper*" OR "respiratory syncytial virus*" OR "bluetongue*")) 
AND 
TITLE-ABS-KEY ( "evidence" OR "sample*" OR "sample collection" OR "swab*" OR "serum" OR "sera" OR "blood" OR "seroprevalence" OR "serolog*" OR "antibod*" OR "seropositiv*" OR "laboratory confirmed" OR "laboratory-confirmed" OR "laboratory positive*" OR "laboratory negative*" OR "specimen*" OR “isolat*" OR "autops*" OR "necrops*" OR "carcass*" OR "case investigation*" OR "epidemiologic investigation*" OR "epidemiological investigation*" OR "virologic investigation*" OR "virological investigation*" OR "pathologic investigation*" OR "pathological investigation*" OR "case series investigation*" OR "case-series investigation*" OR "outbreak investigation*" OR "laboratory investigation*" OR "genom*" OR "sequenc*" OR "phylogen*" )) 
OR 
( TITLE-ABS-KEY ( "case report*" OR "case series" ) AND TITLE-ABS-KEY ( "henipavirus*" OR "nipah*" OR "hendra*" OR "kumasi*" OR "sosuga*" OR "menangle*" OR "filoviridae" OR "filovirus*" OR "ebola*" OR "marburg*" OR "melaka*" OR "pulau*" OR "duvenhage*" )) 
OR
( TITLE-ABS-KEY ( "case report*" OR "case series" ) AND TITLE-ABS-KEY ( "paramyxoviridae" OR "paramyxovirus*" OR "morbillivirus*" OR "respirovirus*" OR "rubulavirus*" OR "orthorubulavirus*" OR "pararubulavirus*" OR "mumps*" OR "reoviridae" OR "reovirus*" OR "orbivirus*" OR "orthoreovirus*" OR "rotavirus*" OR "rhadboviridae" OR "rhabdovirus*" OR "lyssavirus*" OR "rabies*" OR "coronaviridae" OR "coronavirus*" OR "alphacoronavirus*" OR "betacoronavirus*" AND NOT ("covid*" OR "sars-cov-2" OR "coronavirus disease 2019" OR "measles*" OR "peste-des-petits-ruminants*" OR "peste des petits ruminants*" OR "newcastle disease*" OR "rinderpest*" OR "distemper*" OR "respiratory syncytial virus*" OR "bluetongue*")) AND TITLE-ABS-KEY ( "chiroptera" OR "rhinolophus" OR "rousettus" OR "pteropodidae" OR "pteropus" OR "bat" OR "bats" OR "flying fox*" ))
OR 
( TITLE-ABS-KEY ( "covid*" OR "sars-cov-2" OR "coronavirus disease 2019" ) AND TITLE-ABS-KEY ( "chiroptera" OR "rhinolophus" OR "rousettus" OR "pteropodidae" OR "pteropus" OR "bat" OR "bats" OR "flying fox*" ) AND TITLE-ABS-KEY ( "evidence" OR "sample*" OR "sample collection" OR "swab*" OR "serum" OR "sera" OR "blood" OR "seroprevalence" OR "serolog*" OR "antibod*" OR "seropositiv*" OR "laboratory confirmed" OR "laboratory-confirmed" OR "laboratory positive*" OR "laboratory negative*" OR "specimen*" OR “isolat*" OR "autops*" OR "necrops*" OR "carcass*" OR "case investigation*" OR "epidemiologic investigation*" OR "epidemiological investigation*" OR "virologic investigation*" OR "virological investigation*" OR "pathologic investigation*" OR "pathological investigation*" OR "case series investigation*" OR "case-series investigation*" OR "outbreak investigation*" OR "laboratory investigation*" OR "genom*" OR "sequenc*" OR "phylogen*"  ))) 
AND 
( EXCLUDE ( DOCTYPE , "ch” ) OR EXCLUDE ( DOCTYPE , "re" ) OR EXCLUDE ( DOCTYPE , "bk" ))`

const apiUrl = `https://api.elsevier.com/content/search/scopus?query=${encodeURIComponent(
  query,
)}`

const defaultJournal = 'ScopUs'

const getUrl = async (article, crossrefUrl) => {
  const scopusLink = article.link?.find(link => link['@ref'] === 'scopus')
  return scopusLink ? scopusLink['@href'] : crossrefUrl || ''
}

const doImportFromScopus =
  (
    findManuscriptWithUri,
    findManuscriptWithDoi,
    getStubManuscriptObject,
    logger,
    workerArgs,
  ) =>
  async ({ urisAlreadyImporting, doisAlreadyImporting, lastImportDate }) => {
    const { startYear, endYear } = workerArgs

    console.log(`Processing import for ScopUs for ${startYear}-${endYear}`)

    const getArticlesFromScopus = async () => {
      const response = await axios.get(apiUrl, {
        headers: {
          'X-ELS-APIKey': apiKey,
        },
        params: {
          date: `${startYear}-${endYear}`,
        },
      })

      return response.data['search-results'].entry
    }

    const openSearch = await getArticlesFromScopus()

    const newManuscript = []

    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < openSearch.length; i++) {
      const article = openSearch[i]

      // eslint-disable-next-line no-continue
      if (article.error) continue

      const doi = article['prism:doi']
      let crossrefData = {}
      let journal = null

      if (doi) {
        // eslint-disable-next-line no-await-in-loop, no-continue
        if (await findManuscriptWithDoi(doi)) continue
        // eslint-disable-next-line no-await-in-loop
        crossrefData = await getCrossrefDataViaDoi(doi)
        journal = crossrefData.journal
      }

      // eslint-disable-next-line no-await-in-loop
      const url = await getUrl(article, crossrefData.url)

      // eslint-disable-next-line no-continue, no-await-in-loop
      if (await findManuscriptWithUri(url)) continue

      const firstAuthor = article['dc:creator']
      const articleTitle = article['dc:title'] ? article['dc:title'] : ''

      let scopusData = {
        submission: {
          $doi: doi,
          firstAuthor,
          $sourceUri: url,
          journal: journal || defaultJournal,
          // articleURL: url, Removing these fields since $sourceUri will cover the articleURL field
          $abstract: '',
          $title: articleTitle,
        },

        doi: article['prism:doi'],
      }

      if (doi) {
        const { title, topics, publishedDate, abstract } = crossrefData

        scopusData = {
          submission: {
            $doi: doi,
            firstAuthor,
            $sourceUri: url,
            // articleURL: url,
            $abstract: abstract || '',
            topics,
            datePublished: publishedDate,
            journal: journal || defaultJournal,
            $title: title || articleTitle,
          },
          doi,
        }
      }

      newManuscript.push(scopusData)
    }

    if (!newManuscript.length) return []

    return newManuscript
  }

module.exports = doImportFromScopus
