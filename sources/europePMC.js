/* eslint-disable no-await-in-loop, no-restricted-syntax, no-console */
const axios = require('axios')
const { isArray, chunk } = require('lodash')
const { getCrossrefDataViaDoi } = require('../crossref')

const defaultJournal = 'europePMC'

const formatDate = date =>
  new Date(date).toISOString().split('T')[0].replace(/-/g, '/')

const getDateRange = workerArgs => {
  const startDate = new Date(formatDate(workerArgs.startDate))
  const endDate = new Date(formatDate(workerArgs.endDate))
  return [startDate, endDate]
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

/** Return an array of manuscript objects sourced from PubMed, avoiding duplicating any manuscript already in Kotahi */
const doImportFromEuropePMC =
  (
    findManuscriptWithUri,
    findManuscriptWithDoi,
    getStubManuscriptObject,
    logger,
    workerArgs,
  ) =>
  async ({ urisAlreadyImporting, doisAlreadyImporting, lastImportDate }) => {
    const [startDate, endDate] = getDateRange(workerArgs)

    console.log(`Processing import for europePMC for ${startDate} - ${endDate}`)

    const query = `(((TITLE_ABS:"zoonoses” OR TITLE_ABS:"zoonotic" OR TITLE_ABS:"zoonosis" OR TITLE_ABS:"spillover*" OR TITLE_ABS:"outbreak*" OR TITLE_ABS:"cross-species" OR TITLE_ABS:"cross species" OR TITLE_ABS:"inter-species" OR TITLE_ABS:"interspecies" OR TITLE_ABS:”case report*” OR TITLE_ABS:”case series”) AND (TITLE_ABS:"chiroptera" OR TITLE_ABS:"rhinolophus" OR TITLE_ABS:"rousettus" OR TITLE_ABS:"pteropodidae" OR TITLE_ABS:"pteropus" OR TITLE_ABS:"bat" OR TITLE_ABS:"bats" OR TITLE_ABS:"flying fox*" OR TITLE_ABS:"paramyxoviridae" OR TITLE_ABS:"paramyxovirus*" OR TITLE_ABS:"henipavirus*" OR TITLE_ABS:"morbillivirus*" OR TITLE_ABS:"respirovirus*" OR TITLE_ABS:"rubulavirus*" OR TITLE_ABS:"nipah*" OR TITLE_ABS:"hendra*" OR TITLE_ABS:"kumasi*" OR TITLE_ABS:"sosuga*" OR TITLE_ABS:"orthorubulavirus*" OR TITLE_ABS:"pararubulavirus*" OR TITLE_ABS:"mumps*" OR TITLE_ABS:"menangle*" OR TITLE_ABS:"filoviridae" OR TITLE_ABS:"filovirus*" OR TITLE_ABS:"ebola*" OR TITLE_ABS:"marburg*" OR TITLE_ABS:"reoviridae" OR TITLE_ABS:"reovirus*" OR TITLE_ABS:"orbivirus*" OR TITLE_ABS:"orthoreovirus*" OR TITLE_ABS:"rotavirus*" OR TITLE_ABS:"melaka*" OR TITLE_ABS:"pulau*" OR TITLE_ABS:"rhabdoviridae" OR TITLE_ABS:"rhabdovirus*" OR TITLE_ABS:"lyssavirus*" OR TITLE_ABS:"rabies*" OR TITLE_ABS:"duvenhage*" OR TITLE_ABS:"coronaviridae" OR TITLE_ABS:"coronavirus*" OR TITLE_ABS:"alphacoronavirus*" OR TITLE_ABS:"betacoronavirus*" NOT (TITLE_ABS:"covid*" OR TITLE_ABS:"sars-cov-2" OR TITLE_ABS:"measles*" OR TITLE_ABS:"peste-des-petits-ruminants*" OR TITLE_ABS:"peste des petits ruminants*" OR TITLE_ABS:"newcastle disease*" OR TITLE_ABS:"rinderpest*" OR TITLE_ABS:"distemper*" OR TITLE_ABS:"respiratory syncytial virus*" OR TITLE_ABS:"bluetongue*"))) OR (( TITLE_ABS:"covid*" OR TITLE_ABS:"sars-cov-2" ) AND ( TITLE_ABS:"chiroptera" OR TITLE_ABS:"rhinolophus" OR TITLE_ABS:"rousettus" OR TITLE_ABS:"pteropodidae" OR TITLE_ABS:"pteropus" OR TITLE_ABS:"bat" OR TITLE_ABS:"bats" OR TITLE_ABS:"flying fox*" ))) AND (SRC:PPR AND HAS_PUBLISHED_VERSION:N)`

    const sort = 'FIRST_IDATE_D asc'

    const format = 'json'

    const pageSize = 1000

    const resultType = 'core'

    const requestUrl = `https://www.ebi.ac.uk/europepmc/webservices/rest/search?query=${query}&format=${format}&sort_date:y&sort=${sort}&pageSize=${pageSize}&resultType=${resultType}&firstResult=0`

    const getFirstAuthor = authorList => {
      if (!authorList?.author.length) return ''
      const { firstName, lastName, initials } = authorList.author[0]
      return [firstName || initials, lastName].filter(Boolean).join(' ')
    }

    const getAuthors = authorList => {
      if (!authorList?.author) return []
      return authorList.author.map(author => {
        const affiliation =
          author.authorAffiliationDetailsList?.[0]?.affiliation

        return {
          firstName: author.firstName || author.initials,
          lastName: author.lastName,
          affiliation,
          // TODO ORCID is also often available; could grab this in future if submission.$authors supports it
        }
      })
    }

    const getBriefInfo = article => {
      return {
        $doi: article.doi ? article.doi : '',
        $title: article.title,
        $authors: getAuthors(article.authorList),
        datePublished: article.firstPublicationDate,
        server: article.bookOrReportDetails?.publisher,
        $abstract: article.abstractText,
        version: '',
        firstAuthor: getFirstAuthor(article.authorList),
      }
    }

    const getDetailedInfo = crossrefData => {
      if (!crossrefData || Object.keys(crossrefData).length === 0) return {}

      return {
        $title: crossrefData.title,
        topics: crossrefData.topics,
        datePublished: crossrefData.publishedDate,
        $abstract: crossrefData.abstract,
        server: crossrefData.journal,
        firstAuthor: crossrefData.author,
      }
    }

    const getURL = (article, crossrefUrl, server) => {
      const sites = [article.server, 'Europe_PMC', 'arXiv']

      const fullTextURLs =
        article.fullTextUrlList?.fullTextUrl.filter(
          url => url.documentStyle !== 'pdf',
        ) || null

      if (!fullTextURLs) {
        return crossrefUrl
      }

      let siteUrl

      if (sites.includes(server)) {
        siteUrl = fullTextURLs.find(urlSite => urlSite.site === server)
        return siteUrl.url
      }

      for (const site of sites) {
        siteUrl = fullTextURLs.find(urlSite => urlSite.site === site)

        if (siteUrl) {
          return siteUrl.url
        }
      }

      return crossrefUrl
    }

    const getTopics = articleDetailedInfo => {
      if (isArray(articleDetailedInfo.category)) {
        return articleDetailedInfo.category
      }

      if (articleDetailedInfo.category) {
        return [articleDetailedInfo.category]
      }

      return []
    }

    const fetchedManuscripts = []

    let cursorMark = ''

    while (true) {
      const { data } = await axios.get(`${requestUrl}&cursorMark=${cursorMark}`)
      const briefInfoAboutArticles = data.resultList.result

      console.log(`Fetched ${briefInfoAboutArticles.length} articles.`)

      if (briefInfoAboutArticles.length === 0) {
        break
      }

      let breakTheLoop = false

      for (const article of briefInfoAboutArticles) {
        const articleDate = new Date(article.firstIndexDate)

        if (articleDate >= startDate && articleDate <= endDate) {
          fetchedManuscripts.push(article)
        } else if (articleDate > endDate) {
          breakTheLoop = true
          break
        }
      }

      if (breakTheLoop) {
        break
      }

      // Check if there are more pages
      if (data.nextCursorMark) {
        cursorMark = data.nextCursorMark
        sleep(2)
      } else {
        break
      }
    }

    console.log(
      `Total manuscripts for the given dates are ${fetchedManuscripts.length}`,
    )

    const newManuscripts = []

    // Process in batches for querying DB and CrossRef. Mainly to reduce
    // DB load, as the crossref routine has its own internal rate-limiter.
    for (const chunkOfFetchedManuscripts of chunk(fetchedManuscripts, 20)) {
      const newManuscriptsChunk = await Promise.all(
        chunkOfFetchedManuscripts.map(async article => {
          let crossrefData = {}
          const { doi } = article

          if (doi) {
            const manuscriptWithDoi = await findManuscriptWithDoi(doi)
            if (manuscriptWithDoi) return null
            crossrefData = await getCrossrefDataViaDoi(doi)
          }

          const briefInfo = getBriefInfo(article)
          const detailedInfo = getDetailedInfo(crossrefData)

          const detailedArticleData = {
            ...briefInfo,
            ...detailedInfo,
            // EuropePMC seems to give better quality author name info than CrossRef
            firstAuthor: briefInfo.firstAuthor || detailedInfo.firstAuthor,
          }

          const { server } = detailedArticleData

          const url = getURL(article, crossrefData.url, server)

          const manuscriptWithUrl = await findManuscriptWithUri(url)
          if (manuscriptWithUrl) return null

          return {
            submission: {
              ...detailedArticleData,
              journal: server || defaultJournal,
              topics: getTopics(detailedArticleData),
              $sourceUri: url,
              $doi: doi,
            },
          }
        }),
      )

      newManuscripts.push(...newManuscriptsChunk.filter(Boolean))
    }

    return newManuscripts
  }

module.exports = doImportFromEuropePMC
