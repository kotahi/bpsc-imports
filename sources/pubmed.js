/* eslint-disable no-console, no-restricted-syntax */
/* eslint-disable no-continue, no-underscore-dangle, no-await-in-loop */
const axios = require('axios')
const xml2json = require('xml-js')
const FormData = require('form-data')
const fetch = require('node-fetch')
const { cloneDeep, chunk } = require('lodash')
const { getCrossrefDataViaDoi } = require('../crossref')

const ncbiURL = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi`
const detailedArticleInfo = `https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi`

const pubmedQuery = `
((("zoonoses"[mh] OR "zoonoses"[tw] OR "zoonotic"[tw] OR "zoonosis"[tw] OR "spillover*"[tw] OR "outbreak*"[tw] OR "cross-species"[tw] OR "cross species"[tw] OR "inter-species"[tw] OR "interspecies"[tw] OR ("animals"[mh] AND "humans"[mh] AND "chiroptera"[mh])) 
AND 
("chiroptera"[mh] OR "chiroptera"[tw] OR "rhinolophus"[tw] OR "rousettus"[tw] OR "pteropodidae"[tw] OR "pteropus"[tw] OR "bat"[tw] OR "bats"[tw] OR "flying fox*"[tw] OR "paramyxoviridae"[mh:noexp] OR "paramyxoviridae infections"[mh:noexp] OR "henipavirus"[mh] OR "henipavirus infections"[mh] OR "morbillivirus"[mh:noexp] OR "morbillivirus infections"[mh:noexp] OR “respirovirus”[mh:noexp] OR "respirovirus infections"[mh:noexp] OR "rubulavirus"[mh:noexp] OR "rubulavirus infections"[mh:noexp] OR "paramyxovirus*"[tw] OR "henipavirus*"[tw] OR "morbillivirus*"[tw] OR "respirovirus*"[tw] "rubulavirus*"[tw] OR "nipah*"[tw] OR "hendra*"[tw] OR "kumasi*"[tw] OR "sosuga*"[tw] OR "orthorubulavirus*"[tw] OR "pararubulavirus*"[tw] OR "mumps*"[tw] OR "menangle*"[tw] OR "filoviridae"[mh] OR "filoviridae infections"[mh] OR “filovirus*”[tw] OR "ebola*"[tw] OR "marburg*"[tw] OR "reoviridae"[mh:noexp] OR "reoviridae infections"[mh:noexp] OR "orbivirus"[mh:noexp] OR "orthoreovirus"[mh:noexp] OR "rotavirus"[mh] OR “rotavirus infections”[mh] OR "reovirus*"[tw] OR "orbivirus*"[tw] OR "orthoreovirus*"[tw] OR "rotavirus*"[tw] OR "melaka*"[tw] OR "pulau*"[tw] OR "rhabdoviridae"[mh:noexp] OR "rhabdoviridae infections"[mh:noexp] OR “lyssavirus”[mh] OR “rabies”[mh] OR "rhabdovirus*"[tw] OR "lyssavirus*"[tw] OR "rabies*"[tw] OR "duvenhage*"[tw] OR "coronaviridae"[mh:noexp] OR "coronaviridae infections"[mh:noexp] OR “coronavirus”[mh] OR “coronavirus infections”[mh] OR "coronavirus*"[tw] OR "alphacoronavirus*"[tw] OR "betacoronavirus*"[tw] NOT ("covid-19"[mh] OR "sars-cov-2"[mh] OR “measles”[mh] OR “measles virus"[mh] OR "peste-des-petits-ruminants virus"[mh] OR "peste-des-petits-ruminants"[mh] OR "newcastle disease virus"[mh] OR “newcastle disease”[mh] OR “rinderpest virus”[mh] OR “rinderpest”[mh] OR “distemper virus, canine”[mh] OR “distemper virus, phocine”[mh] OR "distemper"[mh] OR "respiratory syncytial viruses”[mh] OR "respiratory syncytial virus infections"[mh] OR "bluetongue virus"[mh] OR "bluetongue"[mh]))
AND 
("evidence"[tw] OR "sample*"[tw] OR "sample collection"[tw] OR "swab*"[tw] OR "serum"[tw] OR "sera"[tw] OR "blood"[tw] OR "seroprevalence"[tw] OR "serolog*"[tw] OR "antibod*"[tw] OR “seropositiv*”[tw] OR “laboratory confirmed"[tw] OR "laboratory-confirmed"[tw] OR "laboratory positive*"[tw] OR "laboratory negative*"[tw] OR "specimen*"[tw] OR "isolat*"[tw] OR "autops*"[tw] OR "necrops*"[tw] OR "carcass*"[tw] OR "case investigation*"[tw] OR "epidemiologic investigation*"[tw] OR "epidemiological investigation*"[tw] OR "virologic investigation*"[tw] OR "virological investigation*"[tw] OR "pathologic investigation*"[tw] OR "pathological investigation*"[tw] OR "case series investigation*"[tw] OR "case-series investigation*"[tw] OR "outbreak investigation*"[tw] OR "laboratory investigation*"[tw] OR “genom*”[tw] OR “sequenc*”[tw] OR "phylogen*"[tw]))
OR 
((“case reports”[pt] OR “case report*”[tw] OR “case series”[tw]) AND ("henipavirus"[mh] OR "henipavirus infections"[mh] OR "henipavirus*"[tw] OR "nipah*"[tw] OR "hendra*"[tw] OR "kumasi*"[tw] OR "sosuga*"[tw] OR "menangle*"[tw] OR "filoviridae"[mh] OR "filoviridae infections"[mh] OR “filovirus*”[tw] OR "ebola*"[tw] OR "marburg*"[tw] OR "melaka*"[tw] OR "pulau*"[tw] OR "duvenhage*"[tw]))
OR 
((“case reports”[pt] OR “case report*”[tw] OR “case series”[tw]) AND ("paramyxoviridae"[mh:noexp] OR "paramyxoviridae infections"[mh:noexp] OR "morbillivirus"[mh:noexp] OR "morbillivirus infections"[mh:noexp] OR “respirovirus”[mh:noexp] OR "respirovirus infections"[mh:noexp] OR "rubulavirus"[mh:noexp] OR "rubulavirus infections"[mh:noexp] OR "paramyxovirus*"[tw] OR "morbillivirus*"[tw] OR "respirovirus*"[tw] OR "rubulavirus*"[tw] OR “orthorubulavirus*"[tw] OR "pararubulavirus*"[tw] OR "mumps*"[tw] OR "reoviridae"[mh:noexp] OR "reoviridae infections"[mh:noexp] OR "orbivirus"[mh:noexp] OR "orthoreovirus"[mh:noexp] OR "rotavirus"[mh] OR “rotavirus infections”[mh] OR "reovirus*"[tw] OR "orbivirus*"[tw] OR "orthoreovirus*"[tw] OR "rotavirus*"[tw] OR "rhabdoviridae"[mh:noexp] OR "rhabdoviridae infections"[mh:noexp] OR “lyssavirus”[mh] OR “rabies”[mh] OR "rhabdovirus*"[tw] OR "lyssavirus*"[tw] OR "rabies*"[tw] OR "coronaviridae"[mh:noexp] OR "coronaviridae infections"[mh:noexp] OR “coronavirus”[mh] OR “coronavirus infections”[mh] OR "coronavirus*"[tw] OR "alphacoronavirus*"[tw] OR "betacoronavirus*"[tw] NOT ("covid-19"[mh] OR "sars-cov-2"[mh] OR “measles”[mh] OR “measles virus"[mh] OR "peste-des-petits-ruminants virus"[mh] OR "peste-des-petits-ruminants"[mh] OR "newcastle disease virus"[mh] OR “newcastle disease”[mh] OR “rinderpest virus”[mh] OR “rinderpest”[mh] OR “distemper virus, canine”[mh] OR “distemper virus, phocine”[mh] OR "distemper"[mh] OR "respiratory syncytial viruses”[mh] OR "respiratory syncytial virus infections"[mh] OR "bluetongue virus"[mh] OR "bluetongue"[mh])) AND ("chiroptera"[mh] OR "chiroptera"[tw] OR "rhinolophus"[tw] OR "rousettus"[tw] OR "pteropodidae"[tw] OR "pteropus"[tw] OR "bat"[tw] OR "bats"[tw] OR "flying fox*"[tw] ))
OR 
(("covid-19"[mh] OR "covid*"[tw] OR "sars-cov-2"[mh] OR "sars-cov-2"[tw]) AND ("chiroptera"[mh] OR "chiroptera"[tw] OR "rhinolophus"[tw] OR "rousettus"[tw] OR "pteropodidae"[tw] OR "pteropus"[tw] OR "bat"[tw] OR "bats"[tw] OR "flying fox*"[tw]) AND ("evidence"[tw] OR "sample*"[tw] OR "sample collection"[tw] OR "swab*"[tw] OR "serum"[tw] OR "sera"[tw] OR "blood"[tw] OR "seroprevalence"[tw] OR "serolog*"[tw] OR "antibod*"[tw] OR “seropositiv*”[tw] OR “laboratory confirmed"[tw] OR "laboratory-confirmed"[tw] OR "laboratory positive*"[tw] OR "laboratory negative*"[tw] OR "specimen*"[tw] OR "isolat*"[tw] OR "autops*"[tw] OR "necrops*"[tw] OR "carcass*"[tw] OR "case investigation*"[tw] OR "epidemiologic investigation*"[tw] OR "epidemiological investigation*"[tw] OR "virologic investigation*"[tw] OR "virological investigation*"[tw] OR "pathologic investigation*"[tw] OR "pathological investigation*"[tw] OR "case series investigation*"[tw] OR "case-series investigation*"[tw] OR "outbreak investigation*"[tw] OR "laboratory investigation*"[tw] OR “genom*”[tw] OR “sequenc*”[tw] OR "phylogen*"[tw])))
NOT 

("review"[ti] OR "review"[pt])`

const defaultJournal = 'PubMed'

const flattenObj = (obj, parent, res = {}) => {
  // eslint-disable-next-line no-restricted-syntax, guard-for-in
  for (const key in obj) {
    const propName = parent ? `${parent}_${key}` : key

    if (typeof obj[key] === 'object') {
      flattenObj(obj[key], propName, res)
    } else {
      res[propName] = obj[key]
    }
  }

  return res
}

const getFirstAuthor = (authorList, crossrefData) => {
  if (!authorList) return crossrefData ? crossrefData.author : ''

  if (!authorList.Author.length) {
    return `${
      authorList.Author.ForeName ? authorList.Author.ForeName._text : ''
    } ${authorList.Author.LastName ? authorList.Author.LastName._text : ''}`
  }

  const author = authorList.Author[0]

  return `${author.ForeName ? author.ForeName._text : ''} ${
    author.LastName ? author.LastName._text : ''
  }`
}

const getTopics = (meshHeadings, crossrefData) => {
  const topics = []

  if (meshHeadings?.MeshHeading) {
    meshHeadings.MeshHeading.forEach(meshHeading => {
      if (meshHeading.DescriptorName._attributes.MajorTopicYN === 'Y') {
        topics.push(meshHeading.DescriptorName._text)
      }
    })
  }

  return crossrefData ? crossrefData.topics : []
}

const getFormattedAbstract = (abstract, crossrefData) => {
  if (!abstract) return crossrefData?.abstract
  if (!abstract.AbstractText.length) return abstract.AbstractText._text

  return abstract.AbstractText.map(
    textWithAttributes =>
      `<p><b>${
        textWithAttributes._attributes
          ? textWithAttributes._attributes.Label
          : ''
      }</b> <br/> ${textWithAttributes._text}</p>`,
  )
    .join('')
    .replace(/\n/gi, '')
}

const formatDateForPubmed = date =>
  new Date(date).toISOString().split('T')[0].replace(/-/g, '/')

const getDateRange = workerArgs => {
  return [
    formatDateForPubmed(workerArgs.startDate),
    formatDateForPubmed(workerArgs.endDate),
  ]
}

const retrieveArticlesFromPubmed = async ids => {
  const formData = new FormData()
  await sleep(3000)
  const idList = ids.join(',')

  const eFetchUrlParameters = {
    db: 'pubmed',
    id: idList,
    tool: 'my_tool',
    email: 'my_email@example.com',
    retmode: 'xml',
  }

  Object.entries(eFetchUrlParameters).map(([key, value]) =>
    formData.append(key, value),
  )

  const idsResponse = await fetch(detailedArticleInfo, {
    method: 'post',
    body: formData,
  }).then(response => response.text())

  const { PubmedArticleSet } = await JSON.parse(
    xml2json.xml2json(idsResponse, {
      compact: true,
      spaces: 2,
    }),
  )

  return PubmedArticleSet
}

const getPubmedDoi = pubMedData => {
  const articleIds = pubMedData.ArticleIdList?.ArticleId

  if (!Array.isArray(articleIds)) {
    return ''
  }

  return articleIds.find(ids => ids._attributes.IdType === 'doi')?._text || ''
}

const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

const getArticleLink = (MedlineCitation, pubMedData, crossrefUrl) => {
  const PMID = MedlineCitation.PMID._text

  if (crossrefUrl) {
    return crossrefUrl
  }

  return `https://pubmed.ncbi.nlm.nih.gov/${PMID}/`
}

/** Return an array of manuscript objects sourced from PubMed, avoiding duplicating any manuscript already in Kotahi */
const doImportFromPubMed =
  (
    findManuscriptWithUri,
    findManuscriptWithDoi,
    getStubManuscriptObject,
    logger,
    workerArgs,
  ) =>
  async ({ urisAlreadyImporting, doisAlreadyImporting, lastImportDate }) => {
    const [minDate, maxDate] = getDateRange(workerArgs)
    const formData = new FormData()
    console.log(`Processing import for PubMed for ${minDate} - ${maxDate}`)

    const eUtilsUrlParameters = {
      retmax: '100000',
      retmode: 'json',
      db: 'pubmed',
      term: pubmedQuery,
      usehistory: 'y',
      mindate: minDate,
      maxdate: maxDate,
      datetype: 'pdate', // publication date is the type of date we limit on
    }

    Object.entries(eUtilsUrlParameters).map(([key, value]) =>
      formData.append(key, value),
    )

    let ids = []

    try {
      const { data } = await axios.post(ncbiURL, formData, {
        headers: formData.getHeaders(),
      })

      ids = data.esearchresult.idlist || []
      logger.info(`Found ${ids.length} candidate manuscripts`)
    } catch (err) {
      logger.error(`Failed to retrieve pubmed data for Query:\n${err.message}`)
    }

    const stub = await getStubManuscriptObject()

    const result = []

    // Don't process too many articles at once, to prevent exhausting the heap 
    for (const idsChunk of chunk(ids, 200)) {
      const PubmedArticleSet = await retrieveArticlesFromPubmed(idsChunk)

      const articles = Array.isArray(PubmedArticleSet.PubmedArticle)
        ? PubmedArticleSet.PubmedArticle
        : [PubmedArticleSet.PubmedArticle]

      // Don't query DB and crossref about too many articles at once.
      // We're querying the DB for duplicates via DOI and via source URI.
      // We're also getting data from CrossRef.
      // The CrossRef function has its own rate-limiter, so it's mainly the DB
      // that we need this chunking for.
      for (const articlesChunk of chunk(articles, 20)) {
        const articlesChunkResult = await Promise.all(
          articlesChunk.map(async article => {
            if (!article) {
              return null
            }

            const { MedlineCitation } = article
            const pubMedData = article.PubmedData

            if (!MedlineCitation) {
              console.log('No MedlineCitation found', MedlineCitation)
              return null
            }

            const doi = getPubmedDoi(pubMedData)

            let crossrefData = {}

            if (doi) {
              if (await findManuscriptWithDoi(doi)) return null
              crossrefData = await getCrossrefDataViaDoi(doi)
            }

            const link = getArticleLink(
              MedlineCitation,
              pubMedData,
              crossrefData.url,
            )

            if (await findManuscriptWithUri(link)) return null

            const PMID = MedlineCitation.PMID._text

            if (!doi) {
              console.log(`DOI not found for article with PubMed ID ${PMID}`)
            }

            const { AuthorList, ArticleTitle, Abstract, Journal } =
              MedlineCitation.Article

            const year = Journal.JournalIssue.PubDate.Year
              ? Journal.JournalIssue.PubDate.Year._text
              : null

            const month = Journal.JournalIssue.PubDate.Month
              ? Journal.JournalIssue.PubDate.Month._text
              : null

            const day = Journal.JournalIssue.PubDate.Day
              ? Journal.JournalIssue.PubDate.Day._text
              : null

            const publishedDate = [year, month, day].filter(Boolean).join('-')
            if (!publishedDate) return null

            // for some titles HTML is returned, need to find _text property in nested object
            const flattedArticleTitle = flattenObj(ArticleTitle)

            // the name of nested property in objects is always _text
            const titlePropName = Object.keys(flattedArticleTitle).find(key =>
              key.includes('_text'),
            )

            const articleTitle = flattedArticleTitle[titlePropName]

            const topics = getTopics(
              MedlineCitation.MeshHeadingList,
              crossrefData,
            )

            let abstract = getFormattedAbstract(Abstract, crossrefData)

            const journal = Journal.Title._text
              ? Journal.Title._text
              : defaultJournal

            if (!abstract) {
              abstract = ''
            }

            return {
              ...cloneDeep(stub),
              submission: {
                ...cloneDeep(stub.submission),
                firstAuthor: getFirstAuthor(AuthorList),
                datePublished: publishedDate,
                $abstract: abstract,
                journal,
                topics,
                $sourceUri: link,
                $doi: doi,
                $title: articleTitle,
              },
              doi,
            }
          }),
        )

        result.push(...articlesChunkResult.filter(Boolean))
      }
    }

    console.log(
      `Found ${result.length} total from pubmed for ${minDate} - ${maxDate}`,
    )

    return result
  }

module.exports = doImportFromPubMed
